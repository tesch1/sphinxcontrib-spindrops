===============
DROPS in Sphinx
===============

.. toctree::
	:hidden:
	


The `sphinx <http://www.sphinx-doc.org>`_ `drops` extension adds two
roles and one directive to ReST processing.  It was used to create
most of the SpinDrops example images on this website.

To enable the extensions, install the drops.py extension, and enable
it in your sphinx :code:`conf.py` file by adding it to the extensions
list::

  extensions = ['drops']


ReST Directive
==============

.. rst:directive:: .. drops:: PTON

   The `drops` directive inserts a DROPS image representing the PTON
   (Plain Text Operator Notation) into the docuemnt.  It takes a
   number of options:

   :code:`:caption:`
      This options can be blank to disable the generation of a caption.
      Otherwise, it contains text (potentially :pton:) to serve as the
      figure's caption
      
   :code:`:align:`
      Causes the image to float and align itself on the page in this direcion,
      valid values are "right" and "left".

   :code:`:sequence: <sequence name>` 
      Select the sequence to be loaded in the simulation.

   :code:`:window: <window name>`
      Open the Window named `<window name>` in SpinDrops, ie List Basis Elems
      or Preferences.

   .. _sphinx_prefs:

   :code:`:prefs:`

      Set preference values in SpinDrops.  This can be a ';'-separated
      list of name=value pairs, where the values are json-parseable
      values, ie :code:`Current Layout=1;Current Separation=0;Show
      Axes=true`

   :code:`:nspin:`
      Set the number of spins in the simulation, can also be
      accomplished with the :code:`prefs` options and setting the
      `"Current Layout"` appropriately.

   :code:`:time:`
      Set the simulation time.

   :code:`:height:`
      The height of the image, either as a px value, or a percentage of
      the width.

   :code:`:width:`
      Set the width of the image, either as a px value, or a percentage of
      the nominal layout width.

   :code:`:aspect:`
      Sets the aspect ratio (width/height) as a percentage.

   :code:`:view:`
      Sets the viewpoint to one of a number of pre-defined positions:
      A,B,C,D.

   :code:`:class:`
      Set the html/css classes of the image, ie, 'border' will add a
      border to the image.

ReST Text Roles
===============

.. rst:role:: pton

   This text role converts a PTON string into a :rst:role:`math`
   element with the PTON replaced by the standard operator notation
   rendered in LaTeX. For example: ``:pton:`I1x``` produces
   :pton:`I1x`

.. rst:role:: drop

   This directive can be used to create an inline DROPS image from a
   PTON string using SpinDrops.  The text in the string can optionally
   contain a number of flags to control the image produced.

   If the :drop: occurs inline in text, a simple :rst:role:`image`
   will be created, whereas if the :drop: is in a table or at the top
   level of the document, a :rst:dir:`figure` with the corresponding
   PTON set as the figure's caption will be produced.

   The role :code:`:drop:`I1x`` produces an inline image: :drop:`@I1x`.

   Some flags are available to modify the DROPS image:
   
   .. + :code:`@` causes the DROPS to be rendered without a figure
   ..   caption.

   + :code:`.scheme` selects the preference ``scheme`` in SpinDrops,
     from the loaded settings, the scheme name must be followed by a
     whitespace character.

   + :code:`|` adds the css class ``border`` to the figure
     
     :code:`:drop:`|I1p`` -> :drop:`|I1p`

   + :code:`^A` :code:`^B` :code:`^C` :code:`^D` changes the viewpoint
     to one of the preset views: A, B, C, or D.
     
     :code:`:drop:`|^AI1p`` -> :drop:`|^AI1p`
     :code:`:drop:`|^BI1p`` -> :drop:`|^BI1p`
     :code:`:drop:`|^CI1p`` -> :drop:`|^CI1p`
     :code:`:drop:`|^DI1p`` -> :drop:`|^DI1p`

   + :code:`{` enables coherence separation in the display.
     
     :code:`:drop:`|^C{I1p`` -> :drop:`|^C{I1p`

   + :code:`/n` changes the image width to (100/n)% of the page, `n`
     must be followed by a space or non-digit character.  For example,
     :code:`/3 I1x` creates an image that is 1/3 of the page width.
     The default image width is 200 px.
     
     :code:`:drop:`|/4I1p`` -> :drop:`|/4I1p`
     :code:`:drop:`|/6I1p`` -> :drop:`|/6I1p`

   + :code:`#n` causes the PTON to be rendered in the context of an
     `n`-spin system.  The value for `n` must be followed by a
     non-digit character character. for example ``:drop:`#2 I1y``` or
     ``:drop:`#2I1x```  If this is unset, the default is to use
     the maximum spin number of the PTON to decide how big the spin
     system should be.
     
     :code:`:drop:`|#2I1p`` -> :drop:`|#2I1p`
     :code:`:drop:`|#3I1p`` -> :drop:`|#3I1p`

   + :code:`"caption"` sets the figure caption to the quoted string.
     Example ``:drop:`"neato"I1x```

     .. appears to be broken
     ..     :code:`:drop:`|"mary had a little lamb"I1p`` -> :drop:`|"mary had a little lamb"I1p`

   + :code:`[...]` sets a prefs string, as in
     the :ref:`:prefs:<sphinx_prefs>` option.

     :code:`:drop:`|[Show Axes=false]I1p`` -> :drop:`|[Show Axes=false]I1p`

